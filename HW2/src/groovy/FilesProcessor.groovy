class FilesProcessor {

    static String FIRST_PATTERN = 'a_'
    static String SECOND_PATTERN = '1_'
    static String THIRD_PATTERN = 'd_'
    static String NEW_NAME = 'done_'
    static String LINE_SEPARATOR = System.getProperty('line.separator')
    static MATRIX_SIZE = 4

    public static void main(String[] args) {
        File currentDir = new File(args[0])
        if (currentDir.exists() && currentDir.isDirectory()) {
            processDir(currentDir)
        }
    }

    static def processFile(File file) {
        String name = file.name
        if (name.startsWith(FIRST_PATTERN))
            firstProcess(file)
        else if (name.startsWith(SECOND_PATTERN))
            secondProcess(file)
        else if (name.startsWith(THIRD_PATTERN))
            thirdProcess(file)

    }

    static def processDir(dir) {
        dir.eachFile { processFile(it) }
        dir.eachDir {
            processDir(it)
        }
    }

    static def firstProcess(File file) {
        Integer number = 0
        String text = file.text
        text.each {
            if (it == FIRST_PATTERN[0])
                number++
        }
        file << LINE_SEPARATOR + number
        rename(file)
    }

    static def secondProcess(File file) {
        def matrix = new int[MATRIX_SIZE][MATRIX_SIZE]
        for (i in 0..MATRIX_SIZE - 1)
            for (j in 0..MATRIX_SIZE - 1)
                matrix[i][j] = new Random().nextInt(100)
        def diagonal = 0
        def subDiagonal = 0
        for (i in 0..MATRIX_SIZE - 1) {
            diagonal += matrix[i][i]
            subDiagonal += matrix[MATRIX_SIZE - 1 - i][i]
        }
        file.write(matrix.toString())
        file << LINE_SEPARATOR + (diagonal)
        file << LINE_SEPARATOR + (subDiagonal)
        rename(file)
    }

    static def thirdProcess(File file) {
        def d = new Date()
        file.write(d.format('yyyy-MM-DD HH-MM-SS'))
        rename(file)
    }

    static def rename(File file) {
        file.renameTo(file.parent + File.separator + NEW_NAME + file.name)
    }

}
