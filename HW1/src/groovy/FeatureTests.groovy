class FeatureTests extends GroovyTestCase {

    class Testing {
        static def complete(String s) {
            return s + ' World!'
        }
    }

    void testCategory() {
        String s = 'Hello'
        use(Testing) {
            assert 'Hello World!' == s.complete()
        }
    }

    void testClosure() {
        def randomNumber = new Random().nextInt(10)
        def createMass = {
            def mass = []
            while (it > 0) {
                mass.push(it--)
            }
            return mass
        }
        assert randomNumber == createMass(randomNumber).size()
    }

    void testString() {
        def last = '9'
        def s = "12345678${last}"
        assert s[8] == '9'
    }
}